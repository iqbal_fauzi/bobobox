package com.bobobox.bobobox.di

import com.bobobox.bobobox.data.repository.local.LocalRepository
import com.bobobox.bobobox.data.repository.remote.RemoteRepository
import org.koin.dsl.module

/**
 * Created by iqbalfauzi on 12/03/20 21.15
 * iqbal.fauzi.if99@gmail.com
 */
val repositoryModule = module {
    factory { RemoteRepository(get(), get(), get(), get(), get()) }
    factory { LocalRepository() }
}