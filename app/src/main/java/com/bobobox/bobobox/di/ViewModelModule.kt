package com.bobobox.bobobox.di

import com.bobobox.bobobox.ui.auth.AuthVM
import com.bobobox.bobobox.ui.main.MainViewModel
import com.bobobox.bobobox.ui.main.dashboard.DashboardViewModel
import com.bobobox.bobobox.ui.main.home.HomeViewModel
import com.bobobox.bobobox.ui.main.notifications.NotificationsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by iqbalfauzi on 12/03/20 13.34
 * iqbal.fauzi.if99@gmail.com
 */
val viewModelModule = module {
    viewModel { AuthVM() }
    viewModel { MainViewModel() }
    viewModel { HomeViewModel() }
    viewModel { DashboardViewModel() }
    viewModel { NotificationsViewModel() }
}