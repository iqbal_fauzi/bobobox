package com.bobobox.bobobox.di

import com.bobobox.bobobox.data.repository.remote.network.NetworkConfig
import org.koin.dsl.module

/**
 * Created by Iqbal Fauzi on 21/03/20 16.33
 * iqbal.fauzi.if99@gmail.com
 */
val networkModule = module {
    single { NetworkConfig.apiBasic() }
    single { NetworkConfig.apiBearer() }
    single { NetworkConfig.onUpdate() }
}