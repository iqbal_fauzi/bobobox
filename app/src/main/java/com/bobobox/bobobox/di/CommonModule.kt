package com.bobobox.bobobox.di

import com.bobobox.bobobox.data.repository.remote.network.SchedulerProvider
import com.bobobox.bobobox.data.session.SessionHelper
import com.bobobox.bobobox.data.session.SharedPreferenceHelper
import org.koin.dsl.module

/**
 * Created by iqbalfauzi on 12/03/20 16.42
 * iqbal.fauzi.if99@gmail.com
 */
val commonModule = module {
    single { SessionHelper() }
    single { SharedPreferenceHelper(get()) }
    single { SchedulerProvider() }
}