package com.bobobox.bobobox.data.constant

/**
 * Created by Iqbal Fauzi on 21/03/20 16.05
 * iqbal.fauzi.if99@gmail.com
 */
object AuthConstant {
    const val GRANT_TYPE = "grant_type"
    const val USERNAME = "username"
    const val PASSWORD = "password"
}