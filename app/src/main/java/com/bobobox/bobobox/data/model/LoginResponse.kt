package com.bobobox.bobobox.data.model

import com.bobobox.bobobox.base.BaseResponse
import com.google.gson.annotations.SerializedName

/**
 * Created by iqbalfauzi on 12/03/20 17.00
 * iqbal.fauzi.if99@gmail.com
 */
class LoginResponse: BaseResponse() {

    @SerializedName("data") val loginResponse: LoginEntity? = null
    @SerializedName("token") val token: TokenResponse? = null

    data class LoginEntity(
        @SerializedName("Username") val username: String? = null,
        @SerializedName("email") val email: String? = null,
        @SerializedName("pathPhoto") val pathPhoto: String? = null
    )

}