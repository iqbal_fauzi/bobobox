package com.bobobox.bobobox.data.repository.remote.network

import com.bobobox.bobobox.base.BaseResponse
import io.reactivex.functions.Consumer

/**
 * Created by iqbalfauzi on 12/03/20 22.11
 * iqbal.fauzi.if99@gmail.com
 */
abstract class ResponseHandler<M : BaseResponse>(private val code: Int?) : Consumer<M> {

    abstract fun onSuccess(model: M)

    abstract fun onError(model: M)

    override fun accept(model: M) {
        when (model.code) {
            code -> onSuccess(model)
            else -> onError(model)
        }
    }

}