package com.bobobox.bobobox.data.constant

/**
 * Created by iqbalfauzi on 12/03/20 13.36
 * iqbal.fauzi.if99@gmail.com
 */


object SessionConstants {
    const val IS_LOGIN = "isLogin"
    const val TOKEN = "token"
}