package com.bobobox.bobobox.data.model.response

import android.os.Parcelable
import com.bobobox.bobobox.base.BaseResponse
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Iqbal Fauzi on 21/03/20 17.13
 * iqbal.fauzi.if99@gmail.com
 */
@Parcelize
data class UpdateResponse(
    @SerializedName("latestVersion") var latestVersion: String? = null,
    @SerializedName("latestVersionCode") var latestVersionCode: Int? = null,
    @SerializedName("url") var updateUrl: String? = null,
    @SerializedName("forceUpdate") var forceUpdate: Boolean,
    @SerializedName("releaseNotes") var releaseNotes: List<String>? = null
) : BaseResponse(), Parcelable