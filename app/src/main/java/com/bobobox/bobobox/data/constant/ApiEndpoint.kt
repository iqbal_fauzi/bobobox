package com.bobobox.bobobox.data.constant

/**
 * Created by iqbalfauzi on 12/03/20 17.08
 * iqbal.fauzi.if99@gmail.com
 */
enum class ApiEndpoint(val value: String) {
    LOGIN("https://api.bobobox.co.id/sign-in")
}