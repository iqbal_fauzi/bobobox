package com.bobobox.bobobox.data.repository.remote.network

import com.bobobox.bobobox.data.model.TokenResponse
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Iqbal Fauzi on 05/04/20 21.39
 * iqbal.fauzi.if99@gmail.com
 */
interface AuthBasicApiInterface {

    @POST("oauth2/token")
    fun getTokenClient(@Body grantType: RequestBody?): Observable<TokenResponse>

    @POST("oauth2/token")
    fun getUserCredential(@Body user: RequestBody?): Observable<TokenResponse>

}