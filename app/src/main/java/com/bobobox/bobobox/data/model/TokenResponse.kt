package com.bobobox.bobobox.data.model

import android.os.Parcelable
import com.bobobox.bobobox.base.BaseResponse
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by iqbalfauzi on 12/03/20 17.02
 * iqbal.fauzi.if99@gmail.com
 */
@Parcelize
data class TokenResponse(
    @SerializedName("access_token") var accessToken: String? = null,
    @SerializedName("token_type") var tokenType: String? = null,
    @SerializedName("expires_in") var expiresIn: Long = 0,
    @SerializedName("refresh_token") var refreshToken: String? = null,
    @SerializedName("scope") var scope: String? = null
) : BaseResponse(), Parcelable