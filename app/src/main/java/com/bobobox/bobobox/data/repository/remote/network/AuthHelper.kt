package com.bobobox.bobobox.data.repository.remote.network

import com.bobobox.bobobox.data.constant.AuthConstant
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Created by Iqbal Fauzi on 21/03/20 16.00
 * iqbal.fauzi.if99@gmail.com
 */
object AuthHelper {

    const val REGISTRATION = "0"
    const val FORGOT_PASSWORD = "1"
    const val PHONE = 0
    const val EMAIL = 1

    fun requestBody(): RequestBody {
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("grant_type", "client_credentials")
            .build()
    }

    fun requestBodyLogin(contact: String, password: String): RequestBody {
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart(AuthConstant.GRANT_TYPE, AuthConstant.PASSWORD)
            .addFormDataPart(AuthConstant.USERNAME, contact)
            .addFormDataPart(AuthConstant.PASSWORD, password)
            .build()
    }

    fun requestBodyAuthCode(
        contact: String,
        contactType: Int?,
        type: String,
        code: String?
    ): RequestBody? {
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("telephone", (if (contactType == PHONE) contact else ""))
            .addFormDataPart("email", (if (contactType == EMAIL) contact else ""))
            .addFormDataPart("code", (code ?: ""))
            .addFormDataPart("type", type)
            .build()
    }

    fun requestBodyDetailMember(
        name: String,
        password: String,
        contactType: Int,
        contact: String,
        contact2: String,
        countryCode: String
    ): RequestBody? {
        val requestBody: RequestBody
        requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("name", name)
            .addFormDataPart("password", password)
            .addFormDataPart(
                "telephone",
                (if (contactType == PHONE) contact else countryCode + contact2)
            )
            .addFormDataPart(
                "email",
                (if (contactType == EMAIL) contact else contact2)
            )
            .addFormDataPart(
                "isEmailVerified",
                if (contactType == EMAIL) "1" else "0"
            )
            .addFormDataPart(
                "isTelephoneVerified",
                if (contactType == PHONE) "1" else "0"
            )
            .build()
        return requestBody
    }

    fun requestBodyResetPassword(
        contactType: Int,
        password: String,
        contact: String
    ): RequestBody? {
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("password", password)
            .addFormDataPart(
                "telephone",
                (if (contactType == PHONE) contact else "")
            )
            .addFormDataPart("email", (if (contactType == EMAIL) contact else ""))
            .addFormDataPart(
                "isEmailVerified",
                if (contactType == EMAIL) "1" else "0"
            )
            .addFormDataPart(
                "isTelephoneVerified",
                if (contactType == PHONE) "1" else "0"
            )
            .build()
    }

}