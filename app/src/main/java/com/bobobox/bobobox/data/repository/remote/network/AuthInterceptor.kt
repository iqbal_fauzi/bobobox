package com.bobobox.bobobox.data.repository.remote.network

import com.bobobox.bobobox.BuildConfig
import com.bobobox.bobobox.data.constant.ApiConstant
import com.bobobox.bobobox.data.session.SessionHelper
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val authType: String) : Interceptor {

    private val sessionHelper = SessionHelper()

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val authHeader = if (authType == ApiConstant.AUTH_BASIC) {
            BASIC_AUTHORIZATION
        } else {
            val token = sessionHelper.getTokenSession()
            "${token?.tokenType} ${token?.accessToken}"
        }
        request = request.newBuilder()
                .addHeader("Authorization", authHeader)
                .addHeader("content-Type", "application/x-www-form-urlencoded")
                .method(request.method, request.body)
                .build()
        return chain.proceed(request)
    }

    companion object {
        private const val BASIC_AUTHORIZATION = "Basic ${BuildConfig.AUTHORIZATION}"
    }

}