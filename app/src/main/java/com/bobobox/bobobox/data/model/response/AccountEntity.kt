package com.bobobox.bobobox.data.model.response

import android.os.Parcelable
import com.bobobox.bobobox.base.BaseResponse
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Iqbal Fauzi on 06/04/20 11.26
 * iqbal.fauzi.if99@gmail.com
 */
@Parcelize
data class AccountEntity(
        @SerializedName("id") var id: Long = 0,
        @SerializedName("name") var name: String? = null,
        @SerializedName("nickName") var nickname: String? = null,
        @SerializedName("telephone") var telephone: String? = null,
        @SerializedName("isTelephoneVerified") var isTelephoneVerified: Boolean = false,
        @SerializedName("email") var email: String? = null,
        @SerializedName("isEmailVerified") var isEmailVerified: Boolean = false,
        @SerializedName("status") var status: Boolean = false,
        @SerializedName("totalGiftCertificate") var totalGiftCertificate: Long = 0,
        @SerializedName("totalGiftCertificateLabel") var totalGiftCertificateLabel: String? = null,
        @SerializedName("details") var detail: AccountDetailEntity? = null,
        @SerializedName("identities") var identities: List<AccountIdentity>? = null,
        @SerializedName("isIdentityVerified") var isIdentityVerified: Boolean = false,
        @SerializedName("urlIdentity") var identityUrl: String? = null,
        @SerializedName("profilePictureUrl") var profilePictureUrl: String? = null,
        @SerializedName("activeCount") var activeCount: Int = 0,
        @SerializedName("upcomingCount") var upcomingCount: Int = 0,
        @SerializedName("pastCount") var pastCount: Int = 0,
        @SerializedName("pendingCount") var pendingCount: Int = 0,
        @SerializedName("stayCount") var stayCount: Int = 0,
        @SerializedName("nightCount") var nightCount: Int = 0
) : BaseResponse(), Parcelable

@Parcelize
data class AccountDetailEntity(
    @SerializedName("id") var id: Long = 0,
    @SerializedName("profilePictureUrl") var profilePictureUrl: String? = null,
    @SerializedName("address") var address: String? = null,
    @SerializedName("gender") var gender: String? = null,
    @SerializedName("birthDate") var birthDate: String? = null,
    @SerializedName("maritalStatus") var maritalStatus: String? = null
) : Parcelable

@Parcelize
data class AccountIdentity(
    @SerializedName("type") var type: String? = null,
    @SerializedName("isVerified") var isVerified: Boolean = false,
    @SerializedName("name") var name: String? = null,
    @SerializedName("url") var url: String? = null
) : Parcelable