package com.bobobox.bobobox.data.session

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.bobobox.bobobox.data.constant.SessionConstants
import com.bobobox.bobobox.data.constant.TokenConstants
import com.bobobox.bobobox.data.model.TokenResponse

/**
 * Created by Iqbal Fauzi on 02/04/20 08.05
 * iqbal.fauzi.if99@gmail.com
 */
class SharedPreferenceHelper(context: Context) {

    private val spName = "boboboxSession"
    private val sharedPref: SharedPreferences = context.getSharedPreferences(spName, Context.MODE_PRIVATE)

    fun addTokenSession(token: TokenResponse) {
        token.let {
            addSession(TokenConstants.ACCESS_TOKEN, it.accessToken.toString())
            addSession(TokenConstants.TOKEN_TYPE, it.tokenType.toString())
            addSession(TokenConstants.EXPIRES_IN, it.expiresIn)
            addSession(TokenConstants.REFRESH_TOKEN, it.refreshToken.toString())
            addSession(TokenConstants.SCOPE, it.scope.toString())
        }
    }

    fun isLogin(): Boolean {
        return getSession(SessionConstants.IS_LOGIN, false)
    }

    fun setLogin(isLogin: Boolean) {
        addSession(SessionConstants.IS_LOGIN, isLogin)
    }

    fun addSession(key: String, value: String) {
        sharedPref.edit { putString(key, value) }
    }

    fun addSession(key: String, value: Boolean) {
        sharedPref.edit { putBoolean(key, value) }
    }

    fun addSession(key: String, value: Int) {
        sharedPref.edit { putInt(key, value) }
    }

    fun addSession(key: String, value: Float) {
        sharedPref.edit { putFloat(key, value) }
    }

    fun addSession(key: String, value: Long) {
        sharedPref.edit { putLong(key, value) }
    }

    fun getSession(key: String, defaultValue: String): String? {
        return sharedPref.getString(key, defaultValue)
    }

    fun getSession(key: String, defaultValue: Boolean): Boolean {
        return sharedPref.getBoolean(key, defaultValue)
    }

    fun getSession(key: String, defaultValue: Int): Int? {
        return sharedPref.getInt(key, defaultValue)
    }

    fun getSession(key: String, defaultValue: Float): Float? {
        return sharedPref.getFloat(key, defaultValue)
    }

}