package com.bobobox.bobobox.data.constant

/**
 * Created by iqbalfauzi on 16/03/20 21.17
 * iqbal.fauzi.if99@gmail.com
 */
object ApiConstant {

    const val AUTH_BASIC = "basic"
    const val AUTH_BEARER = "bearer"

}