package com.bobobox.bobobox.data.repository.remote.network

import com.bobobox.bobobox.data.model.TokenResponse
import com.bobobox.bobobox.data.model.response.AccountEntity
import com.bobobox.bobobox.data.model.response.DataResponse
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by Iqbal Fauzi on 05/04/20 21.39
 * iqbal.fauzi.if99@gmail.com
 */
interface ApiInterface {

    @POST("oauth2/token")
    fun getTokenClient(@Body grantType: RequestBody?): Observable<TokenResponse>

    @GET("clients/v1/my-account")
    fun getUserInformation(): Observable<DataResponse<AccountEntity>>

}