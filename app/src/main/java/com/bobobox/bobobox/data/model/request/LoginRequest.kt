package com.bobobox.bobobox.data.model.request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by iqbalfauzi on 12/03/20 17.06
 * iqbal.fauzi.if99@gmail.com
 */
@Parcelize
data class LoginRequest(
    @SerializedName("username") val username: String? = null,
    @SerializedName("phoneNumber") val phone: String? = null,
    @SerializedName("email") val email: String? = null,
    @SerializedName("password") val password: String
) : Parcelable