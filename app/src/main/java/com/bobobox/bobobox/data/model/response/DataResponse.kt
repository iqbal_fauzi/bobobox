package com.bobobox.bobobox.data.model.response

import com.bobobox.bobobox.base.BaseResponse
import com.google.gson.annotations.SerializedName

/**
 * Created by Iqbal Fauzi on 06/04/20 11.24
 * iqbal.fauzi.if99@gmail.com
 */
data class DataResponse<T>(
    @SerializedName("data") var data: T? = null
) : BaseResponse()