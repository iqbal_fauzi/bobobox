package com.bobobox.bobobox.data.repository.remote

import com.bobobox.bobobox.base.BaseResponse
import com.bobobox.bobobox.data.model.TokenResponse
import com.bobobox.bobobox.data.model.response.AccountEntity
import com.bobobox.bobobox.data.model.response.DataResponse
import com.bobobox.bobobox.data.model.response.UpdateResponse
import com.bobobox.bobobox.data.repository.remote.network.*
import com.bobobox.bobobox.data.session.SessionHelper
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Iqbal Fauzi on 21/03/20 17.16
 * iqbal.fauzi.if99@gmail.com
 */
open class RemoteRepository(
    private val apiUpdate: UpdateApiInterface,
    private val apiBasic: AuthBasicApiInterface,
    private val apiBearer: AuthBearerApiInterface,
    private val schedulerProvider: SchedulerProvider,
    private val sessionHelper: SessionHelper
) {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getUserCredential(
        contact: String,
        password: String,
        callback: OnResponseCallback<TokenResponse>
    ) {
        val request = AuthHelper.requestBodyLogin(contact, password)
        compositeDisposable.add(
            apiBasic.getUserCredential(request)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    callback.onSuccess(it)
                }, {
                    callback.onError(it)
                })
        )
    }

    fun getUserInformation(callback: OnResponseCallback<DataResponse<AccountEntity>>) {
        compositeDisposable.add(
            apiBearer.getUserInformation()
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    callback.onSuccess(it)
                }, {
                    callback.onError(it)
                })
        )
    }

    fun onRequestToken(callback: OnResponseCallback<TokenResponse>) {
        val token = sessionHelper.getTokenSession()
        if (token == null) {
            compositeDisposable.add(
                apiBasic.getTokenClient(AuthHelper.requestBody())
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({
                        callback.onSuccess(it)
                    }, {
                        callback.onError(it)
                    })
            )
        }
    }

    fun onCheckUpdate(): Observable<UpdateResponse> {
        return apiUpdate.getForceUpdate()
            .flatMap { Observable.just(it) }
            .onErrorResumeNext { err: Throwable ->
                return@onErrorResumeNext Observable.error<UpdateResponse?>(err)
            }
    }

    interface OnResponseCallback<M : BaseResponse> {
        fun onSuccess(response: M)
        fun onError(t: Throwable)
    }

}