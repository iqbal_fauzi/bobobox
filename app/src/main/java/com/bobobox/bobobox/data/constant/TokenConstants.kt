package com.bobobox.bobobox.data.constant

/**
 * Created by iqbalfauzi on 12/03/20 13.36
 * iqbal.fauzi.if99@gmail.com
 */
object TokenConstants {
    const val ACCESS_TOKEN = "access_token_key"
    const val TOKEN_TYPE = "token_type_key"
    const val EXPIRES_IN = "expires_in_key"
    const val REFRESH_TOKEN = "refresh_token_key"
    const val SCOPE = "scope"
}