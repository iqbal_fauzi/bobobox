package com.bobobox.bobobox.data.repository.remote.network

import com.bobobox.bobobox.utils.logger.AppLogger
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.Buffer
import java.io.IOException

class LoggingInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val t1 = System.nanoTime()
        var requestLog = String.format("Sending request %s on %s%n%s",
                request.url, chain.connection(), request.headers)
        //YLog.d(String.format("Sending request %s on %s%n%s",
        // request.url(), chain.connection(), request.headers()));
        if (request.method.compareTo("post", ignoreCase = true) == 0) {
            requestLog = """
                $requestLog
                ${bodyToString(request)}
                """.trimIndent()
        }
        AppLogger.d("TAG", "request\n$requestLog")
        val response = chain.proceed(request)
        val t2 = System.nanoTime()
        val responseLog = String.format("Received response for %s in %.1fms%n%s",
                response.request.url, (t2 - t1) / 1e6, response.headers)
        val bodyString = response.body?.string()
        AppLogger.d("TAG", """
     response
     $responseLog
     $bodyString
     ${response.code}
     """.trimIndent())
        return response.newBuilder()
                .body(bodyString.toString().toResponseBody(response.body?.contentType()))
                .build()
    }

    companion object {
        fun bodyToString(request: Request): String {
            return try {
                val copy = request.newBuilder().build()
                val buffer = Buffer()
                copy.body?.writeTo(buffer)
                buffer.readUtf8()
            } catch (e: IOException) {
                "did not work"
            }
        }
    }
}