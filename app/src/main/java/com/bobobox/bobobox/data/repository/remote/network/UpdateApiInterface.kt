package com.bobobox.bobobox.data.repository.remote.network

import com.bobobox.bobobox.data.model.response.UpdateResponse
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Iqbal Fauzi on 21/03/20 15.58
 * iqbal.fauzi.if99@gmail.com
 */
interface UpdateApiInterface {
    @GET("bobobox-production-assets/assets/android/update-changelog.json")
    fun getForceUpdate(): Observable<UpdateResponse>
}