package com.bobobox.bobobox.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.bobobox.bobobox.utils.view.snackBarRed
import com.bobobox.bobobox.utils.view.snackbarAccent
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.reflect.KClass

/**
 * Created by iqbalfauzi on 11/03/20 22.30
 * iqbal.fauzi.if99@gmail.com
 */
abstract class BaseActivity<out V : BaseViewModel, D : ViewDataBinding>(klazz: KClass<V>) : AppCompatActivity() {

    val viewModel : V by viewModel(klazz)

    protected lateinit var dataBinding: D
    protected val fm = supportFragmentManager
    var rootView : View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, setLayout())
        rootView = dataBinding.root
        Timber.tag(javaClass.simpleName)
        onInitUI(savedInstanceState)
        showMessage()
        showErrorMessage()
    }

    fun showErrorMessage() {
        viewModel.errorMessage.observe(this, Observer {
            rootView?.snackBarRed(it, this)
        })
    }

    fun showMessage() {
        viewModel.message.observe(this, Observer {
            rootView?.snackbarAccent(it, this)
        })
    }

    fun gotoActivity(c: Class<*>, bundle: Bundle?, isFinish: Boolean) {
        val i = Intent(this, c)
        if (bundle != null) {
            i.putExtras(bundle)
        }
        startActivity(i)
        if (isFinish) {
            finish()
        }
    }

    fun gotoActivityClearStack(c: Class<*>, bundle: Bundle?) {
        val i = Intent(this, c)
        if (bundle != null) {
            i.putExtras(bundle)
        }
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(i)
        finish()
    }

    fun showDialogFragment(dialogClass: Class<*>, data: Bundle?) {
        val fm = supportFragmentManager
        val fragment : DialogFragment?

        fragment = dialogClass.newInstance() as DialogFragment
        fragment.apply {
            arguments = data
            show(fm, fragment::class.java.simpleName)
        }
    }

    abstract fun onInitUI(savedInstanceState: Bundle?)

    abstract fun setLayout(): Int

}