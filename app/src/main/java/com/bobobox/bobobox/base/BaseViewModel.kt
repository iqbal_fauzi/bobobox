package com.bobobox.bobobox.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bobobox.bobobox.BuildConfig
import com.bobobox.bobobox.data.repository.remote.RemoteRepository
import com.bobobox.bobobox.data.session.SessionHelper
import com.bobobox.bobobox.utils.logger.AppLogger
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.HttpException
import javax.net.ssl.HttpsURLConnection

/**
 * Created by iqbalfauzi on 12/03/20 11.21
 * iqbal.fauzi.if99@gmail.com
 */
abstract class BaseViewModel : ViewModel(), KoinComponent {
    //secondary constructor

    val sessionHelper: SessionHelper by inject()
    val remoteRepository: RemoteRepository by inject()
    var gson = Gson()
    val API_STATUS_CODE_LOCAL_ERROR = 0

    val onRequest: MutableLiveData<Boolean> = MutableLiveData()
    val message: MutableLiveData<String> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()

    fun handleApiError(error: Throwable) {
        when (error) {
            is HttpException -> {
                when (error.code()) {
                    HttpsURLConnection.HTTP_UNAUTHORIZED -> errorMessage.postValue("Unauthorized User")
                    HttpsURLConnection.HTTP_FORBIDDEN -> errorMessage.postValue("Forbidden")
                    HttpsURLConnection.HTTP_INTERNAL_ERROR -> errorMessage.postValue("Internal Server Error")
                    HttpsURLConnection.HTTP_BAD_REQUEST -> errorMessage.postValue("Bad Request")
                    API_STATUS_CODE_LOCAL_ERROR -> errorMessage.postValue("No Internet Connection")
                    else -> errorMessage.postValue(error.getLocalizedMessage())
                }
            }
            is RuntimeException -> {
                errorMessage.postValue(error.message)
            }
            is JsonSyntaxException -> {
                errorMessage.postValue("Something Went Wrong API is not responding properly!")
            }
            else -> {
                errorMessage.postValue(error.message)
            }
        }
    }

    fun logging(message: String) {
        if (BuildConfig.DEBUG) {
            AppLogger.i(message)
            this.message.value = message
        }
    }

}