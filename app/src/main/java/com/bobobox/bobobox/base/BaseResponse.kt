package com.bobobox.bobobox.base

import com.google.gson.annotations.SerializedName

/**
 * Created by iqbalfauzi on 12/03/20 11.11
 * iqbal.fauzi.if99@gmail.com
 */
open class BaseResponse {
//    @SerializedName("status") var status: Int? = null
    @SerializedName("code") var code: Int? = null
    @SerializedName("message") var message: String? = null
}