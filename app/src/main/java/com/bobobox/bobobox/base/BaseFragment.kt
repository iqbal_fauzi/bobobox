package com.bobobox.bobobox.base

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bobobox.bobobox.utils.view.snackBarRed
import com.bobobox.bobobox.utils.view.snackbarAccent
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass


/**
 * Created by iqbalfauzi on 12/03/20 16.16
 * iqbal.fauzi.if99@gmail.com
 */
abstract class BaseFragment<out V: BaseViewModel, D: ViewDataBinding>(klazz: KClass<V>) : Fragment() {

    val viewModel: V by viewModel(klazz)
    val sharedViewModel: V by sharedViewModel(klazz)

    protected lateinit var dataBinding: D
    var dataReceived: Bundle? = null
    var rootView : View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = DataBindingUtil.inflate(inflater, setLayout(), container, false)
        dataReceived = arguments
        rootView = dataBinding.root
        onInitUI(savedInstanceState)
        showMessage()
        showErrorMessage()
        return rootView
    }

    fun goToActivity(c: Class<*>, bundle: Bundle?, isFinish: Boolean) {
        val i = Intent(requireContext(), c)
        if (bundle != null) {
            i.putExtras(bundle)
        }
        startActivity(i)
        if (isFinish) {
            activity?.finish()
        }
    }

    fun goToActivityClearAllStack(c: Class<*>, bundle: Bundle?) {
        val i = Intent(requireContext(), c)
        if (bundle != null) {
            i.putExtras(bundle)
        }
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(i)
        activity?.finish()
    }

    fun showErrorMessage() {
        viewModel.errorMessage.observe(requireActivity(), Observer {
            rootView?.snackBarRed(it, requireContext())
        })
    }

    fun showMessage() {
        viewModel.message.observe(requireActivity(), Observer {
            rootView?.snackbarAccent(it, requireContext())
        })
    }

    abstract fun onInitUI(savedInstanceState: Bundle?)

    abstract fun setLayout(): Int
}