package com.bobobox.bobobox.base

import android.app.Application
import com.bobobox.bobobox.di.commonModule
import com.bobobox.bobobox.di.networkModule
import com.bobobox.bobobox.di.repositoryModule
import com.bobobox.bobobox.di.viewModelModule
import com.bobobox.bobobox.utils.logger.AppLogger
import com.orhanobut.hawk.Hawk
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Created by iqbalfauzi on 12/03/20 11.18
 * iqbal.fauzi.if99@gmail.com
 */
class BoboboxApp : Application() {

    override fun onCreate() {
        super.onCreate()
        // Initialize Hawk(Session)
        Hawk.init(this).build()
        // Initialize Logger
        AppLogger.init()
        // Pretty Logger
        Logger.addLogAdapter(AndroidLogAdapter())
        // Initialize Koin Dependency Injection
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@BoboboxApp)
            modules(listOf(commonModule, viewModelModule, repositoryModule, networkModule))
        }
    }

}