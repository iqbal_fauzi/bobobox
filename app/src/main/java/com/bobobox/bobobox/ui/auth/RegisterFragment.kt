package com.bobobox.bobobox.ui.auth

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseFragment
import com.bobobox.bobobox.databinding.FragmentRegisterBinding

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : BaseFragment<AuthVM, FragmentRegisterBinding>(
    AuthVM::class) {

    override fun onInitUI(savedInstanceState: Bundle?) {

    }

    override fun setLayout(): Int {
        return R.layout.fragment_register
    }

}
