package com.bobobox.bobobox.ui.auth

import android.os.Bundle
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseActivity
import com.bobobox.bobobox.databinding.ActivityAuthBinding

class AuthActivity : BaseActivity<AuthVM, ActivityAuthBinding>(AuthVM::class) {

    override fun onInitUI(savedInstanceState: Bundle?) {

    }

    override fun setLayout(): Int {
        return R.layout.activity_auth
    }
}
