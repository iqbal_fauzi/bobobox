package com.bobobox.bobobox.ui.main.notifications

import android.os.Bundle
import androidx.lifecycle.Observer
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseFragment
import com.bobobox.bobobox.databinding.FragmentNotificationsBinding

class NotificationsFragment : BaseFragment<NotificationsViewModel, FragmentNotificationsBinding>(NotificationsViewModel::class) {

    override fun onInitUI(savedInstanceState: Bundle?) {
        with(dataBinding) {
            viewModel.text.observe(this@NotificationsFragment, Observer {
                textNotifications.text = it
            })
        }
    }

    override fun setLayout(): Int {
        return R.layout.fragment_notifications
    }
}
