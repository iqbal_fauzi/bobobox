package com.bobobox.bobobox.ui.main.home

import android.os.Bundle
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseFragment
import com.bobobox.bobobox.databinding.FragmentHomeBinding

class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>(HomeViewModel::class) {

    override fun onInitUI(savedInstanceState: Bundle?) {
        with(dataBinding) {

        }
    }

    override fun setLayout(): Int {
        return R.layout.fragment_home
    }
}
