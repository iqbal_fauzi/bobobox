package com.bobobox.bobobox.ui.auth

import android.os.Bundle
import android.os.Handler
import androidx.core.os.postDelayed
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseFragment
import com.bobobox.bobobox.databinding.FragmentSplashBinding
import com.bobobox.bobobox.ui.main.MainActivity
import com.bobobox.bobobox.utils.view.snackbarAccent

/**
 * A simple [Fragment] subclass.
 */
class SplashFragment : BaseFragment<AuthVM, FragmentSplashBinding>(AuthVM::class) {

    private var navAuthController : NavController? = null

    override fun onInitUI(savedInstanceState: Bundle?) {
        setupData()
        navAuthController = Navigation.findNavController(requireActivity(), R.id.nav_auth)
        Handler().postDelayed(delayInMillis = 2000) {
            if (viewModel.sessionHelper.isLogin()) {
                goToActivityClearAllStack(MainActivity::class.java, null)
            } else {
                navAuthController?.navigate(SplashFragmentDirections.splashToLogin())
            }
        }
    }

    private fun setupData() {
        with(viewModel) {
            onRequestToken()
            onCheckUpdate()
            updateData.observe(this@SplashFragment, Observer {
                rootView?.snackbarAccent(it.latestVersion.toString(), requireContext())
            })
        }
    }

    override fun setLayout(): Int {
        return R.layout.fragment_splash
    }

}
