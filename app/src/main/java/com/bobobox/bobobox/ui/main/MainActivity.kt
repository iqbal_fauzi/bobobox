package com.bobobox.bobobox.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseActivity
import com.bobobox.bobobox.databinding.ActivityMainBinding
import com.bobobox.bobobox.ui.main.dashboard.DashboardFragment
import com.bobobox.bobobox.ui.main.home.HomeFragment
import com.bobobox.bobobox.ui.main.notifications.NotificationsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>(MainViewModel::class) {

    val fragment1 = HomeFragment()
    val fragment2 = DashboardFragment()
    val fragment3 = NotificationsFragment()
    var active: Fragment = fragment1

    private val mOnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.nav_home -> {
                        fm.beginTransaction().hide(active).show(fragment1).commit()
                        active = fragment1
                        return true
                    }
                    R.id.nav_dashboard -> {
                        fm.beginTransaction().hide(active).show(fragment2).commit()
                        active = fragment2
                        return true
                    }
                    R.id.nav_notif -> {
                        fm.beginTransaction().hide(active).show(fragment3).commit()
                        active = fragment3
                        return true
                    }
                }
                return false
            }
        }

    override fun onInitUI(savedInstanceState: Bundle?) {
        dataBinding.navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        fm.beginTransaction()
            .add(R.id.fragment_container, fragment3, getString(R.string.title_notifications))
            .hide(fragment3).commit()
        fm.beginTransaction()
            .add(R.id.fragment_container, fragment2, getString(R.string.title_dashboard))
            .hide(fragment2).commit()
        fm.beginTransaction()
            .add(R.id.fragment_container, fragment1, getString(R.string.title_home)).commit()
    }

    override fun setLayout(): Int {
        return R.layout.activity_main
    }
}
