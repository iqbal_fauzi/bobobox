package com.bobobox.bobobox.ui.auth

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import androidx.core.os.postDelayed
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseFragment
import com.bobobox.bobobox.databinding.FragmentLoginBinding
import com.bobobox.bobobox.ui.main.MainActivity
import com.bobobox.bobobox.utils.view.hide
import com.bobobox.bobobox.utils.view.show
import com.bobobox.bobobox.utils.view.toggleFade

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : BaseFragment<AuthVM, FragmentLoginBinding>(AuthVM::class) {

    private val loginWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            with(dataBinding) {
                val email = etEmail.text.toString().trim()
                val password = etPassword.text.toString().trim()
                btnLogin.isEnabled =
                    (email.isNotEmpty() && email.length > 5) && (password.isNotEmpty() && password.length > 3)
                btnLogin.run {
                    if (isEnabled) setBackgroundResource(R.drawable.btn_selector_orange) else setBackgroundResource(
                        R.drawable.round_corner_gray
                    )
                }
            }
        }
    }

    override fun onInitUI(savedInstanceState: Bundle?) {
        setupWatcher()
        setupContactChanger()
        setupLogin()
    }

    private fun setupLogin() {
        with(dataBinding) {
            btnLogin.setOnClickListener {
                pbLogin.show()
                viewModel.getUserCredential(etEmail.text.toString(), etPassword.text.toString())
            }
            viewModel.loginData.observe(this@LoginFragment, Observer {
                if (it != null) {
                    pbLogin.hide()
                    goToActivityClearAllStack(MainActivity::class.java, null)
                }
            })
        }
    }

    private fun setupContactChanger() {
        with(dataBinding) {
            tvContactChange.setOnClickListener {
                if (inputEmail.isVisible) {
                    inputEmail.toggleFade(300)
                    Handler().postDelayed(300) {
                        inputPhoneNumber.toggleFade(300)
                    }
                } else {
                    inputPhoneNumber.toggleFade(300)
                    Handler().postDelayed(300) {
                        inputEmail.toggleFade(300)
                    }
                }
            }
        }
    }

    private fun setupWatcher() {
        with(dataBinding) {
            loginWatcher.let {
                etEmail.addTextChangedListener(it)
                etPassword.addTextChangedListener(it)
                btnLogin.addTextChangedListener(it)
            }
        }
    }

    override fun setLayout(): Int {
        return R.layout.fragment_login
    }

}
