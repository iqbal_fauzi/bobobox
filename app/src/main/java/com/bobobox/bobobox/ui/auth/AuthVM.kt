package com.bobobox.bobobox.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bobobox.bobobox.base.BaseViewModel
import com.bobobox.bobobox.data.model.TokenResponse
import com.bobobox.bobobox.data.model.response.AccountEntity
import com.bobobox.bobobox.data.model.response.DataResponse
import com.bobobox.bobobox.data.model.response.UpdateResponse
import com.bobobox.bobobox.data.repository.remote.RemoteRepository
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by iqbalfauzi on 12/03/20 13.47
 * iqbal.fauzi.if99@gmail.com
 */
class AuthVM : BaseViewModel() {

    val loginData: MutableLiveData<AccountEntity> = MutableLiveData()
    val updateData: MutableLiveData<UpdateResponse> = MutableLiveData()
    var disposable: Disposable? = null

//    val contact = ""
//    val password = ""

    override fun onCleared() {
        super.onCleared()
        remoteRepository.compositeDisposable.dispose()
    }

    fun getUserCredential(contact: String, password: String) {
        remoteRepository.getUserCredential(contact, password, object : RemoteRepository.OnResponseCallback<TokenResponse> {
            override fun onSuccess(response: TokenResponse) {
                getUserInformation()
            }

            override fun onError(t: Throwable) {
                handleApiError(t)
            }

        })
    }

    fun getUserInformation() : LiveData<AccountEntity> {
        remoteRepository.getUserInformation(object : RemoteRepository.OnResponseCallback<DataResponse<AccountEntity>> {
            override fun onSuccess(response: DataResponse<AccountEntity>) {
                loginData.value = response.data
            }

            override fun onError(t: Throwable) {
                handleApiError(t)
            }
        })
        return loginData
    }

    fun onRequestToken() {
//        disposable = remoteRepository.onRequestToken()
//            .subscribeOn(Schedulers.io())
//            .subscribe({
//
//            }, {
//
//            })
        remoteRepository.onRequestToken(object : RemoteRepository.OnResponseCallback<TokenResponse> {
            override fun onSuccess(response: TokenResponse) {
                sessionHelper.setLogin(true)
                sessionHelper.addTokenSession(response)
            }
            override fun onError(t: Throwable) {
                handleApiError(t)
            }
        })
    }

    fun onCheckUpdate() {
        disposable = remoteRepository.onCheckUpdate()
            .subscribeOn(Schedulers.io())
            .subscribe({
                updateData.postValue(it)
            }, {
                handleApiError(it)
            })
    }

}