package com.bobobox.bobobox.ui.main.dashboard

import android.os.Bundle
import androidx.lifecycle.Observer
import com.bobobox.bobobox.R
import com.bobobox.bobobox.base.BaseFragment
import com.bobobox.bobobox.databinding.FragmentDashboardBinding

class DashboardFragment : BaseFragment<DashboardViewModel, FragmentDashboardBinding>(DashboardViewModel::class) {

    override fun onInitUI(savedInstanceState: Bundle?) {
        setupData()
    }

    private fun setupData() {
        with(dataBinding) {
            viewModel.text.observe(this@DashboardFragment, Observer {
                textDashboard.text = it
            })
        }
    }

    override fun setLayout(): Int {
        return R.layout.fragment_dashboard
    }
}
