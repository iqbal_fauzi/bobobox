package com.bobobox.bobobox.utils.common

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

/**
 * Created by iqbalfauzi on 12/03/20 16.24
 * iqbal.fauzi.if99@gmail.com
 */
object CommonUtils {

    val localeId: Locale
    get() = Locale("id", "ID")

    fun isJSONValid(test: String?): Boolean {

        if (test == null || test.isEmpty()) {
            return false
        }

        try {
            JSONObject(test)
        } catch (ex: JSONException) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                JSONArray(test)
            } catch (ex1: JSONException) {
                return false
            }

        }

        return true
    }

    fun getPriceFormat(locale: Locale, price: Double?): String {
        val currencyFormat = NumberFormat.getCurrencyInstance(locale)

        return currencyFormat.format(price)
    }

    fun getSplittedString(text: String, regex: String): Array<String> {
        return text.split(regex.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    }

}