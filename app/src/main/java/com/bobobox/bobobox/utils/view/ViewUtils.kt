package com.bobobox.bobobox.utils.view

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.bobobox.bobobox.R
import com.google.android.material.snackbar.Snackbar


/**
 * Created by iqbalfauzi on 12/03/20 16.34
 * iqbal.fauzi.if99@gmail.com
 */
fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun Context.toast(message: String, time: Int, position: Int) {
    /*
    * time is duration of toast 0 = short and 1 = long
    * position 0 = default/bottom, 1 = center, 2 = top
    * */
    if (time == 0 && position == 0) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.BOTTOM, 0, 24)
        toast.show()
    } else if (time == 1 && position == 0) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.BOTTOM, 0, 0)
        toast.show()
    } else if (time == 0 && position == 1) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    } else if (time == 0 && position == 2) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.TOP, 0, 0)
        toast.show()
    } else if (time == 1 && position == 1) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    } else {
        val toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.TOP, 0, 0)
        toast.show()
    }
}

fun toggleUpDownWithAnimation(view: View): Boolean {
    return if (view.rotation == 0f) {
        view.animate().setDuration(150).rotation(180f)
        true
    } else {
        view.animate().setDuration(150).rotation(0f)
        false
    }
}

fun Snackbar.configSnackbar(context: Context) {
    val params = this.view.layoutParams as ViewGroup.MarginLayoutParams
    params.setMargins(12, 12, 12, 12)
    this.view.apply {
        layoutParams = params
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            background = context.getDrawable(R.drawable.bg_snackbar)
        }
    }
    ViewCompat.setElevation(this.view, 6f)

}

fun View.snackbarWhite(message: String, context: Context) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    val sbView = snackbar.view
    val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
    textView.setTextColor(Color.RED)
    snackbar.apply {
        configSnackbar(context)
        show()
    }
}

fun View.snackbarAccent(message: String, context: Context) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_SHORT)
    val sbView = snackbar.view
    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent))
    val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    snackbar.show()
}

fun View.snackBarRed(message: String, context: Context) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    val sbView = snackbar.view
    val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
    snackbar.show()
}