package com.bobobox.bobobox.utils.common

import android.content.Context
import android.net.ParseException
import com.bobobox.bobobox.R
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by iqbalfauzi on 12/03/20 16.27
 * iqbal.fauzi.if99@gmail.com
 */
object DateTimeUtils {

    // END EPOCH CONVERTER

    // DATE COMPONENT
    val currentDate: String
        get() = "$currentDay/$currentMonth/$currentYear"

    val currentTime: String
        get() = "$currentHour:$currentMinute:$currentSecond"

    val currentDateTime: String
        get() = "$currentDate $currentTime"

    val currentDay: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.DAY_OF_MONTH)
        }

    // January = 0
    val currentMonth: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.MONTH) + 1
        }

    val currentMonthDatePicker: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.MONTH)
        }

    val currentYear: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.YEAR)
        }

    val currentHour: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.HOUR_OF_DAY)
        }

    val currentMinute: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.MINUTE)
        }

    val currentSecond: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.SECOND)
        }

    // END DATE COMPONENT

    // Calendar CONVERTER

    val todayInCalendar: Calendar
        get() = Calendar.getInstance()

    val lastDateOfMonth: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        }

    fun getCalculatedDateByToday(dateFormat: String, days: Int): String {
        val cal = Calendar.getInstance()
        val s = SimpleDateFormat(dateFormat, Locale.getDefault())
        cal.add(Calendar.DAY_OF_YEAR, days)
        //        System.out.println("Hitung : " + s.format(new Date(cal.getTimeInMillis())));
        return s.format(Date(cal.timeInMillis))
    }

    fun getCalculatedDateByDate(date: String, dateFormat: String, days: Int): String? {
        val cal = Calendar.getInstance()
        val s = SimpleDateFormat(dateFormat, Locale.getDefault())
        cal.add(Calendar.DAY_OF_YEAR, days)
        try {
            return s.format(Date(s.parse(date).time))
        } catch (e: ParseException) {
            Timber.e("Error in Parsing Date : %s", e.printStackTrace())
        }
        return null
    }

    fun getAge(context: Context, tglLahir: String): String {
        val birth = Calendar.getInstance()
        val now = Calendar.getInstance()
        val currentTime = System.currentTimeMillis()
        now.timeInMillis = currentTime

        val parsing = tglLahir.split("/")
        val birthDate = parsing[0].toInt()
        val birthMonth = parsing[1].toInt()
        val birthYear = parsing[2].toInt()
        birth.set(birthYear, birthMonth, birthDate)

        var year = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR)
        val curMonth = now.get(Calendar.MONTH) + 1
        var months = curMonth - birthMonth
        val days: Int

        if (months < 0) {
            year--
            months = 12 - birthMonth + curMonth
            if (now.get(Calendar.DATE) < birth.get(Calendar.DATE)) {
                months--
            }
        } else if (months == 0 && now.get(Calendar.DATE) < birth.get(Calendar.DATE)) {
            year--
            months = 11
        }

        if (now.get(Calendar.DATE) > birth.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birth.get(Calendar.DATE)
        else if (now.get(Calendar.DATE) < birth.get(Calendar.DATE)) {
            val today = now.get(Calendar.DAY_OF_MONTH)
            now.add(Calendar.MONTH, -1)
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birth.get(Calendar.DAY_OF_MONTH) + today
        } else {
            days = 0
            if (months == 12) {
                year++
                months = 0
            }
        }

        val tahun = "$year ${context.resources.getString(R.string.tahun)}"


        val bulan = "$months ${context.resources.getString(R.string.bulan)}"

        val hari = "$days ${context.resources.getString(R.string.hari)}"

        return "$tahun $bulan $hari"
    }

    // EPOCH CONVERTER
    fun epochToDate(epoch: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epoch * 1000
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)

        return (if (day < 10) "0$day" else day).toString() + "/" + (if (month < 10) "0$month" else month) + "/" + year
    }

    fun epochToTime(epoch: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epoch * 1000
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        return (if (hour < 10) "0$hour" else hour).toString() + ":" + if (minute < 10) "0$minute" else minute
    }

    fun epochToDateTime(epoch: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epoch * 1000
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        return ((if (day < 10) "0$day" else day).toString() + "/" + (if (month < 10) "0$month" else month) + "/" + year + " "
                + (if (hour < 10) "0$hour" else hour) + ":" + if (minute < 10) "0$minute" else minute)
    }

    fun epochToHumanDate(epoch: Long?): String {
        return SimpleDateFormat("EEEE, dd MMMM yyyy", CommonUtils.localeId)
            .format(epoch?.times(1000)?.let { Date(it) })
    }

    fun epochToReadableDate(epoch: Long): String {
        return SimpleDateFormat("dd MMM yyyy", CommonUtils.localeId)
            .format(Date(epoch * 1000))
    }

    fun epochToHumanDateTime(epoch: Long): String {
        return SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm", CommonUtils.localeId)
            .format(Date(epoch * 1000))
    }

    fun convertToHumanDate(value: String): String {
        var humanDate = ""
        val date: Array<String>

        if (value.contains(":")) {
            val dateTime = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            date = dateTime[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        } else {
            date = value.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }

        val dateInEpoch = dateToEpoch(date[2] + "/" + date[1] + "/" + date[0])
        humanDate = epochToHumanDate(dateInEpoch)

        return humanDate
    }

    fun convertToReadableDate(value: String): String {
        var humanDate = ""
        val date: Array<String>

        if (value.contains(":")) {
            val dateTime = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            date = dateTime[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        } else {
            date = value.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }

        val dateInEpoch = dateToEpoch(date[2] + "/" + date[1] + "/" + date[0])
        humanDate = epochToReadableDate(dateInEpoch)

        return humanDate
    }

    fun convertToHumanDateTime(value: String): String {
        var humanDate = ""

        val dateTime = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val date = dateTime[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val time = dateTime[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val dateInEpoch = dateTimeToEpoch(
            date[2] + "/" + date[1] + "/" + date[0] + " "
                    + time[0] + ":" + time[1]
        )
        humanDate = epochToHumanDateTime(dateInEpoch)

        return humanDate
    }

    fun dateToEpoch(date: String): Long {
        val splitDate = CommonUtils.getSplittedString(date, "/")

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitDate[0]))
        calendar.set(Calendar.MONTH, Integer.parseInt(splitDate[1]) - 1)
        calendar.set(Calendar.YEAR, Integer.parseInt(splitDate[2]))
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        return calendar.timeInMillis / 1000
    }

    fun dateTimeToEpoch(dateTime: String): Long {
        val splitDateTime = CommonUtils.getSplittedString(dateTime, " ")
        val splitDate = CommonUtils.getSplittedString(splitDateTime[0], "/")
        val splitTime = CommonUtils.getSplittedString(splitDateTime[1], ":")

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitDate[0]))
        calendar.set(Calendar.MONTH, Integer.parseInt(splitDate[1]) - 1)
        calendar.set(Calendar.YEAR, Integer.parseInt(splitDate[2]))
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitTime[0]))
        calendar.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]))

        return calendar.timeInMillis / 1000
    }

    fun getDateTimeInCalendar(day: Int?, month: Int?, year: Int?, hour: Int?, minute: Int?): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, day!!)
        calendar.set(Calendar.MONTH, month!! - 1)
        calendar.set(Calendar.YEAR, year!!)
        calendar.set(Calendar.HOUR_OF_DAY, hour!!)
        calendar.set(Calendar.MINUTE, minute!!)

        return calendar
    }

    fun getDateInCalendar(day: Int?, month: Int?, year: Int?): Calendar {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, day!!)
        calendar.set(Calendar.MONTH, month!! - 1)
        calendar.set(Calendar.YEAR, year!!)

        return calendar
    }

    //format date 28/11/1995
    fun dateToCalendar(date: String): Calendar {
        val splitDate = CommonUtils.getSplittedString(date, "/")
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitDate[0]))
        calendar.set(Calendar.MONTH, Integer.parseInt(splitDate[1]) - 1)
        calendar.set(Calendar.YEAR, Integer.parseInt(splitDate[2]))

        return calendar
    }

    //format datetime 28/11/1995 19:00
    fun getDateTimeInCalendar(dateTime: String): Calendar {
        val datetimeSplit = CommonUtils.getSplittedString(dateTime, " ")
        val dateSplit = CommonUtils.getSplittedString(datetimeSplit[0], "/")
        val timeSplit = CommonUtils.getSplittedString(datetimeSplit[1], ":")

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, Integer.parseInt(dateSplit[2]))
        calendar.set(Calendar.MONTH, Integer.parseInt(dateSplit[1]) - 1)
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateSplit[0]))
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeSplit[0]))
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeSplit[1]))

        return calendar
    }

    fun getAddedDayCalendar(date: String, addDay: Int): Calendar {
        val calendar = dateToCalendar(date)
        calendar.add(Calendar.DAY_OF_MONTH, addDay)

        return calendar
    }

    fun getAddedMonthCalendar(date: String, addMonth: Int): Calendar {
        val calendar = dateToCalendar(date)
        calendar.add(Calendar.MONTH, addMonth)

        return calendar
    }

    fun getAddedYearCalendar(date: String, addYear: Int): Calendar {
        val calendar = dateToCalendar(date)
        calendar.add(Calendar.YEAR, addYear)

        return calendar
    }

    fun countDate(tglAwal: Long?, tglAkhir: Long?): Long? {
        val tglMulai = epochToDate(tglAwal!!)
        val tglSelesai = epochToDate(tglAkhir!!)

        val startCalendar = dateToCalendar(tglMulai)
        val endCalendar = dateToCalendar(tglSelesai)

        val dateInMilis = startCalendar.timeInMillis - endCalendar.timeInMillis

        return dateInMilis / (24 * 60 * 60 * 1000)
    }

    // END Calendar CONVERTER

    fun getMonth(position: Int): String {
        val months = arrayOf(
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "Oktober",
            "November",
            "December"
        )

        return months[position]
    }

    fun getDayOfWeek(position: Int): String {
        val dayOfWeek = arrayOf("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu")

        return dayOfWeek[position]
    }

    fun getHour(hour: Int?): String {
        var textHour = ""

        if (hour!! < 10) {
            textHour = "0" + hour
        } else {
            textHour = hour.toString()
        }

        return textHour
    }

    fun getMinute(minute: Int?): String {
        var textMinute = ""

        if (minute!! < 10) {
            textMinute = "0" + minute
        } else {
            textMinute = minute.toString()
        }

        return textMinute
    }

}