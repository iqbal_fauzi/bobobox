package com.bobobox.bobobox.utils.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.core.view.isVisible

/**
 * Created by Iqbal Fauzi on 04/04/20 14.34
 * iqbal.fauzi.if99@gmail.com
 */
fun View.toggleFade(duration: Long) {
    if (this.isVisible) {
        this.animate()
            .alpha(0.0f)
            .setDuration(duration)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    this@toggleFade.hide()
                }
            })
    } else {
        this.animate()
            .alpha(1.0f)
            .setDuration(duration)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationStart(animation: Animator?) {
                    super.onAnimationStart(animation)
                    this@toggleFade.show()
                }
            })
    }
}

fun getProgressBarAnimation(): AnimationDrawable {

    val rainbow1 = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
        intArrayOf(Color.RED, Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW))

    val rainbow2 = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
        intArrayOf(Color.YELLOW, Color.RED, Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN))

    val rainbow3 = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
        intArrayOf(Color.GREEN, Color.YELLOW, Color.RED, Color.MAGENTA, Color.BLUE, Color.CYAN))

    val rainbow4 = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
        intArrayOf(Color.CYAN, Color.GREEN, Color.YELLOW, Color.RED, Color.MAGENTA, Color.BLUE))

    val rainbow5 = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
        intArrayOf(Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.RED, Color.MAGENTA))

    val rainbow6 = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
        intArrayOf(Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.RED))


    val gds = arrayListOf(rainbow1, rainbow2, rainbow3, rainbow4, rainbow5, rainbow6)

    val animation = AnimationDrawable()

    for (gd in gds) {
        animation.addFrame(gd, 100)
    }

    animation.isOneShot = false;

    return animation;
}