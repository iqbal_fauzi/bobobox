package com.bobobox.bobobox.utils.logger

import android.util.Log
import com.bobobox.bobobox.utils.logger.FakeCrashLibrary.logError
import com.bobobox.bobobox.utils.logger.FakeCrashLibrary.logWarning
import timber.log.Timber


/**
 * Created by Iqbal Fauzi on 14:44 21/11/19
 */
class CrashReportingTree : Timber.Tree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }
        FakeCrashLibrary.log(priority, tag, message)
        if (t != null) {
            if (priority == Log.ERROR) {
                logError(t)
            } else if (priority == Log.WARN) {
                logWarning(t)
            }
        }
    }
}