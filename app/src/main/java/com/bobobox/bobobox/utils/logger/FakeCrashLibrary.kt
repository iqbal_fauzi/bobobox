package com.bobobox.bobobox.utils.logger

/**
 * Created by iqbalfauzi on 12/03/20 13.29
 * iqbal.fauzi.if99@gmail.com
 */
object FakeCrashLibrary {

    fun log(priority: Int, tag: String?, message: String?) {
        // TODO add log entry to circular buffer.
    }

    fun logWarning(t: Throwable?) {
        // TODO report non-fatal warning.
    }

    fun logError(t: Throwable?) {
        // TODO report non-fatal error.
    }

    private fun fakeCrashLibrary() {
        throw AssertionError("No instances.")
    }

}